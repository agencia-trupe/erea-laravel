import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(document).ready(function () {
  // BANNERS
  $(".banners").cycle({
    slides: ".banner",
  });

  // SUBMENU PRODUTOS
  $("#menuProdutos, .submenu-categorias").mouseenter(function () {
    $(".submenu-categorias").css("display", "flex");
  });
  $("#menuProdutos, .submenu-categorias").mouseleave(function () {
    $(".submenu-categorias").css("display", "none");
  });

  // SUBMENU COLECOES
  $("#menuColecoes, .submenu-colecoes").mouseenter(function () {
    $(".submenu-colecoes").css("display", "flex");
  });
  $("#menuColecoes, .submenu-colecoes").mouseleave(function () {
    $(".submenu-colecoes").css("display", "none");
  });

  // GRID CLIPPING + BTN VER MAIS
  var $grid = $(".masonry-grid").masonry({
    itemSelector: ".grid-item",
    columnWidth: ".grid-item",
    percentPosition: true,
    isAnimated: true,
  });
  $grid.masonry("once", "layoutComplete", function (event, laidOutItems) {
    $(".masonry-grid").masonry("layout");
  });

  var itensClipping = $(".clipping");
  var spliceItensQuantidade = 10;

  if (itensClipping.length <= spliceItensQuantidade) {
    $(".btn-clipping").hide();
  }

  var setDivClipping = function () {
    var spliceItens = itensClipping.splice(0, spliceItensQuantidade);
    $(".masonry-grid").append(spliceItens);
    $(spliceItens).show();
    if ($(spliceItens[0]).length > 0) {
      $("html,body .masonry-grid").animate(
        { scrollTop: $(spliceItens[0]).offset().top },
        1000
      );
    }
    $(".masonry-grid").masonry("layout");
    if (itensClipping.length <= 0) {
      $(".btn-clipping").hide();
    }
  };

  $(".btn-clipping").click(function () {
    setDivClipping();
  });

  $(".clipping").hide();
  setDivClipping();

  // PRODUTOS/VARIAÇÕES
  var itensVariacoes = $(".variacoes .variacao");

  if (itensVariacoes.length <= 3) {
    $(".variacoes .nav").hide();
  } else {
    $(".itens-variacoes").slick({
      dots: false,
      vertical: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      verticalSwiping: true,
      infinite: false,
      prevArrow: $(".nav-up"),
      nextArrow: $(".nav-down"),
    });
  }

  itensVariacoes.click(function (e) {
    e.preventDefault();
    $(".variacoes .variacao.variacao-ativo").removeClass("variacao-ativo");
    $(this).addClass("variacao-ativo");
    var src = $(this).attr("href");
    $(".detalhes-img img").attr("src", src);
  });

  // ANIMAÇÃO TROCA CATEGORIA
  $(".submenu-categorias .categoria").click(function (e) {
    e.preventDefault();

    $(".submenu-categorias .categoria.active").removeClass("active");
    $(this).addClass("active");

    getProdutos(this.href);
  });

  // ANIMAÇÃO TROCA COLEÇÕES
  $(".submenu-colecoes .colecao").click(function (e) {
    e.preventDefault();

    $(".submenu-colecoes .colecao.active").removeClass("active");
    $(this).addClass("active");

    getProdutos(this.href);
  });

  var getProdutos = function (url) {
    var pagDetalhe = window.location.pathname.indexOf("detalhe") > -1;

    var urlPrevia = "";
    // var urlPrevia = "/previa-erea";

    if (pagDetalhe) {
      // remove /list e manda para rota de produtos/categorias
      window.location.href = url.replace("/list", "");
    } else {
      // troca de categoria via ajax
      $(".produtos").fadeOut("slow", function () {
        $.ajax({
          type: "GET",
          url: url,
          beforeSend: function () {
            $(".pt-categ").hide();
            $(".pt-colecao").hide();
            $(".loading").show();
          },
          success: function (data, textStatus, jqXHR) {
            $(".pt-categ").html("");
            $(".pt-colecao").html("");
            $(".produtos").html("").show();

            if (data.categoria) {
              $(".pt-categ").append(data.categoria.titulo);
              window.history.pushState(
                "",
                "",
                urlPrevia + "/produtos/" + data.categoria.slug
              );
            }
            if (data.colecao) {
              $(".pt-colecao").append(data.colecao.titulo);
              window.history.pushState(
                "",
                "",
                urlPrevia + "/colecoes/" + data.colecao.slug
              );
            }

            var time = 0;

            data.produtos.forEach((element) => {
              if (element.capa_alternativa != null) {
                var html =
                  "<a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/produtos/detalhe/" +
                  element.slug +
                  "' class='produto' title='" +
                  element.titulo +
                  "' id='" +
                  element.id +
                  "' style='display:none; opacity:0;'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/produtos/colecoes/" +
                  element.capa_alternativa +
                  "' alt='" +
                  element.titulo +
                  "'></a>";
              } else {
                var html =
                  "<a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/produtos/detalhe/" +
                  element.slug +
                  "' class='produto' title='" +
                  element.titulo +
                  "' id='" +
                  element.id +
                  "' style='display:none; opacity:0;'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/produtos/" +
                  element.imagem +
                  "' alt='" +
                  element.titulo +
                  "'></a>";
              }

              $(".produtos").append(html);
              time += 200;
              setTimeout(function () {
                $("#" + element.id).show();
                $("#" + element.id).animate({ opacity: 1 }, "slow");
              }, time);
            });
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
          },
          complete: function () {
            $(".loading").hide();
            $(".pt-categ").show();
            $(".pt-colecao").show();
          },
        });
      });
    }
  };

  // AVISO DE COOKIES
  $(".aviso-cookies").hide();

  if (window.location.href == routeHome) {
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
      var url = window.location.origin + "/aceite-de-cookies";

      $.ajax({
        type: "POST",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $(".aviso-cookies").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  }
});
