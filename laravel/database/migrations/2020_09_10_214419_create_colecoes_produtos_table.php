<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateColecoesProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('colecoes_produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('colecao_id')->unsigned();
            $table->foreign('colecao_id')->references('id')->on('colecoes')->onDelete('cascade');
            $table->integer('produto_id')->unsigned();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('capa_alternativa')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('colecoes_produtos');
    }
}
