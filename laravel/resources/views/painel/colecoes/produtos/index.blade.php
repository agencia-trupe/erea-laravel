@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.colecoes.index') }}" title="Voltar para Coleções" class="btn btn-sm btn-default">
    &larr; Voltar para Coleções </a>

<legend>
    <h2>
        <small>Coleções / Produtos da Coleção:</small> {{ $colecao->titulo }}
        <a href="{{ route('painel.colecoes.produtos.create', $colecao->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produtos</a>
    </h2>
</legend>

@if(!count($produtosColecao))
<div class="alert alert-warning" role="alert">Nenhum produto encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="colecoes_produtos">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Produto</th>
            <th>Categoria</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @for($i = 0; $i < count($produtos); $i++)
        <tr class="tr-row" id="{{ $produtosC[$i] }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $produtos[$i]->titulo }}</td>
            <td>{{ $categorias->find($produtos[$i]->categoria_id)->titulo }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.colecoes.produtos.destroy', 'id' => $colecao->id, 'produto' => $produtos[$i]->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endfor
    </tbody>
</table>
@endif

@endsection