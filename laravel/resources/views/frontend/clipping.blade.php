@extends('frontend.common.template')

@section('content')

<div class="content clippings">
    <div class="center masonry-grid">
        @foreach($clippings as $clipping)
        <a href="{{ route('clipping.show', $clipping->id) }}" class="clipping grid-item">
            <img src="{{ asset('assets/img/clipping/capa/'.$clipping->imagem) }}" alt="">
            <p class="titulo">{{ $clipping->titulo }}</p>
        </a>
        @endforeach
    </div>
    <button class="btn-clipping">ver mais +</button>
</div>

@endsection