<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(InstitucionalTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(ColecoesTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
    }
}
