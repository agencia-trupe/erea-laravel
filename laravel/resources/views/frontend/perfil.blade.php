@extends('frontend.common.template')

@section('content')

<div class="content perfil">
    <img src="{{ asset('assets/img/institucional/'.$institucional->imagem) }}" alt="">
    <div class="perfil-texto">
        {!! $institucional->texto !!}
    </div>
</div>

@endsection