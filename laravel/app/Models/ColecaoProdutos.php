<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ColecaoProdutos extends Model
{
    protected $table = 'colecoes_produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeColecao($query, $id)
    {
        return $query->where('colecao_id', $id);
    }

    public function produtos()
    {
        return $this->hasMany('App\Models\Produto', 'produto_id')->ordenados();
    }

    public static function uploadAlternativa()
    {
        return CropImage::make('capa_alternativa', [
                'width'   => 600,
                'height'  => null,
                'upsize'  => true,
                'path'    => 'assets/img/produtos/colecoes/'
        ]);
    }
}
