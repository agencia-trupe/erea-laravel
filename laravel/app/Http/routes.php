<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('perfil', 'PerfilController@index')->name('perfil');
    Route::get('produtos', 'ProdutosController@index')->name('produtos');
    Route::get('produtos/{categoria}', 'ProdutosController@show')->name('produtos.show');
    Route::get('produtos/list/{categoria}', 'ProdutosController@getProdutos')->name('produtos.getProdutos');
    Route::get('produtos/detalhe/{produto}', 'ProdutosController@showProduto')->name('produtos.showProduto');
    Route::get('colecoes', 'ColecoesController@index')->name('colecoes');
    Route::get('colecoes/{colecao}', 'ColecoesController@show')->name('colecoes.show');
    Route::get('colecoes/list/{colecao}', 'ColecoesController@getProdutos')->name('colecoes.getProdutos');
    Route::get('clipping', 'ClippingController@index')->name('clipping');
    Route::get('clipping/{id}', 'ClippingController@show')->name('clipping.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::resource('banners', 'BannersController');
        Route::resource('institucional', 'InstitucionalController', ['only' => ['index', 'update']]);
        Route::resource('categorias', 'CategoriasController');
        Route::resource('colecoes', 'ColecoesController');
        Route::get('colecoes/{id}/produtos', 'ColecoesProdutosController@index')->name('painel.colecoes.produtos.index');
        Route::get('colecoes/{id}/produtos/create', 'ColecoesProdutosController@create')->name('painel.colecoes.produtos.create');
        Route::get('produtos/categoria/{categoria_id}', 'ColecoesProdutosController@getProdutoCategoria')->name('painel.colecoes.produtos.getProdutoCategoria');
        Route::post('colecoes/{id}/produtos/create', 'ColecoesProdutosController@store')->name('painel.colecoes.produtos.store');
        Route::delete('colecoes/{id}/produtos/delete/{produto}', 'ColecoesProdutosController@destroy')->name('painel.colecoes.produtos.destroy');
        Route::resource('produtos', 'ProdutosController');
        Route::get('produtos/{produtos}/variacoes/clear', [
            'as'   => 'painel.produtos.variacoes.clear',
            'uses' => 'ProdutosVariacoesController@clear'
        ]);
        Route::resource('produtos.variacoes', 'ProdutosVariacoesController', ['parameters' => ['variacoes' => 'variacoes_produtos']]);
        Route::resource('clipping', 'ClippingController');
        Route::get('clipping/{clipping}/imagens/clear', [
            'as'   => 'painel.clipping.imagens.clear',
            'uses' => 'ClippingImagensController@clear'
        ]);
        Route::resource('clipping.imagens', 'ClippingImagensController', ['parameters' => ['imagens' => 'imagens_clipping']]);
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');

        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
