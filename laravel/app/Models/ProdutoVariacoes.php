<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoVariacoes extends Model
{
    protected $table = 'produtos_variacoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProjeto($query, $id)
    {
        return $query->where('projeto_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 120,
                'height'  => 120,
                'path'    => 'assets/img/produtos/thumbs/'
            ],
            [
                'width'  => 600,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/produtos/'
            ]
        ]);
    }
}
