<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });

        view()->composer('frontend.common.*', function ($view) {
            $view->with('contato', \App\Models\Contato::first());
            $view->with('categorias', \App\Models\Categoria::orderBy('titulo', 'asc')->get());
            $view->with('colecoes', \App\Models\Colecao::orderBy('titulo', 'desc')->get());
        });

        view()->composer('painel.common.*', function ($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });
    }

    public function register()
    {
        setlocale(LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese');
    }
}
