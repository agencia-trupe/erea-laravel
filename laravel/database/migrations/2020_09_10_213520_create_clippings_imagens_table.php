<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateClippingsImagensTable extends Migration
{
    public function up()
    {
        Schema::create('clippings_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->integer('clipping_id')->unsigned();
            $table->foreign('clipping_id')->references('id')->on('clippings')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clippings_imagens');
    }
}
