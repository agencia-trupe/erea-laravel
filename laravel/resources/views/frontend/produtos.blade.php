@extends('frontend.common.template')

@section('content')

<div class="content produtos">
    <div class="center">
        @foreach($categorias as $categoria)
        <a href="{{ route('produtos.show', $categoria->slug) }}" class="categoria">{{ $categoria->titulo }}</a>
        @endforeach
    </div>
</div>

@endsection