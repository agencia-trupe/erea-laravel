@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Categorias /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.categorias.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
