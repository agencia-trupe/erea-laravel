@extends('frontend.common.template')

@section('content')

<div class="content politica-de-privacidade">
    <h2 class="titulo">POLÍTICA DE PRIVACIDADE</h2>
    <div class="texto">
        {!! $politica->texto !!}
    </div>
</div>

@endsection