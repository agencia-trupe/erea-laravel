<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categoria;
use App\Models\Colecao;
use App\Models\ColecaoProdutos;
use App\Models\Produto;

class ColecoesProdutosController extends Controller
{
    public function index($id)
    {
        $colecao = Colecao::find($id);

        $produtosColecao = ColecaoProdutos::where('colecao_id', $colecao->id)->ordenados()->get();

        $produtos = [];
        $produtosC = [];

        foreach($produtosColecao as $p) {
            $produtos[] = Produto::where('id', $p->produto_id)->first();
            $produtosC[] = $p->id;
        }

        $categorias = Categoria::all();

        return view('painel.colecoes.produtos.index', compact('produtos', 'colecao', 'categorias', 'produtosColecao', 'produtosC'));
    }

    public function create($id)
    {
        $colecao = Colecao::find($id);

        $categorias = Categoria::orderBy('titulo', 'asc')->get();

        return view('painel.colecoes.produtos.create', compact('colecao', 'categorias'));
    }

    public function getProdutoCategoria($categoria_id)
    {
        $produtos = Produto::where('categoria_id', $categoria_id)->ordenados()->get();

        return response()->json($produtos);
    }

    public function store(ColecaoProdutos $registro, Request $request)
    {
        try {
            $input['colecao_id'] = $request->colecao_id;
            $input['produto_id'] = $request->produto_id;
            $input['capa_alternativa'] = $request->capa_alternativa;

            if (isset($input['capa_alternativa'])) $input['capa_alternativa'] = ColecaoProdutos::uploadAlternativa();

            $registro = ColecaoProdutos::create($input);

            return redirect()->route('painel.colecoes.produtos.index', $request->colecao_id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($id, $produto)
    {
        try {
            $registro = ColecaoProdutos::where('produto_id', $produto)->where('colecao_id', $id)->first();

            if ($registro) {
                $registro->delete();
            }
            return redirect()->route('painel.colecoes.produtos.index', $id)->with('success', 'Produto excluído com sucesso.');
        } catch (\Exception $e) {
            
            return back()->withErrors(['Erro ao excluir produto: ' . $e->getMessage()]);
        }
    }
}
