<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function variacoes()
    {
        return $this->hasMany('App\Models\ProdutoVariacoes', 'produto_id')->ordenados();
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 120,
                'height'  => 120,
                'path'    => 'assets/img/produtos/thumbs/'
            ],
            [
                'width'  => 600,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/produtos/'
            ]
        ]);
    }
}
