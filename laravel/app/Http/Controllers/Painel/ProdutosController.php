<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutosRequest;
use App\Models\Categoria;
use App\Models\Produto;

class ProdutosController extends Controller
{
    public function index()
    {
        $categorias = Categoria::orderBy('titulo', 'asc')->get();

        if (isset($_GET['categoria'])) {
            $categoria = $_GET['categoria'];
            $produtos = Produto::where('categoria_id', $categoria)->ordenados()->get();
        } else {
            $produtos = Produto::orderBy('titulo', 'asc')->get();
        }

        return view('painel.produtos.index', compact('produtos', 'categorias'));
    }

    public function create()
    {
        $categorias = Categoria::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.produtos.create', compact('categorias'));
    }

    public function store(ProdutosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Produto::upload_imagem();

            Produto::create($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Produto $produto)
    {
        $categorias = Categoria::orderBy('titulo', 'asc')->pluck('titulo', 'id');
        
        return view('painel.produtos.edit', compact('produto', 'categorias'));
    }

    public function update(ProdutosRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Produto::upload_imagem();

            $produto->update($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Produto $produto)
    {
        try {

            $produto->delete();

            return redirect()->route('painel.produtos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
