<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Colecao;
use App\Models\ColecaoProdutos;
use App\Models\Produto;
use App\Models\ProdutoVariacoes;

class ProdutosController extends Controller
{
    public function index()
    {
        $categorias = Categoria::orderBy('titulo', 'asc')->get();

        return view('frontend.produtos', compact('categorias'));
    }

    public function show($categoria)
    {
        $categoriaFind = Categoria::where('slug', $categoria)->first();
        $produtos = Produto::where('categoria_id', $categoriaFind->id)->ordenados()->get();

        return view('frontend.produtos-categoria', compact('produtos', 'categoriaFind'));
    }

    public function getProdutos($categoria)
    {
        $categoriaFind = Categoria::where('slug', $categoria)->first();
        $produtos = Produto::where('categoria_id', $categoriaFind->id)->ordenados()->get();

        return response()->json(['produtos' => $produtos, 'categoria' => $categoriaFind]);
    }

    public function showProduto($produto)
    {
        $produtoDetalhe = Produto::where('slug', $produto)->first();
        $categoriaFind = Categoria::find($produtoDetalhe->categoria_id);
        
        // $colecaoFind = ColecaoProdutos::where('produto_id', $produto)->first();
        // $colecao = Colecao::find($colecaoFind->colecao_id);

        $variacoes = ProdutoVariacoes::where('produto_id', $produtoDetalhe->id)->ordenados()->get();

        return view('frontend.produtos-detalhes', compact('categoriaFind', 'produtoDetalhe', 'variacoes'));
    }
}
