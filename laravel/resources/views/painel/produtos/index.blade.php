@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Produtos
        <a href="{{ route('painel.produtos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto</a>
    </h2>
</legend>

<h4 class="titulo-categorias">Categorias:</h4>
<div class="lista-categorias">
    @foreach($categorias as $categoria)
    <a href="{{ route('painel.produtos.index', ['categoria' => $categoria->id]) }}" class="categoria {{ (isset($_GET['categoria']) && $_GET['categoria'] == $categoria->id) ? 'active' : '' }}">{{ $categoria->titulo }}</a>
    @endforeach
</div>

@if(!count($produtos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="produtos">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Título</th>
            <th>Imagens</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($produtos as $produto)
        <tr class="tr-row" id="{{ $produto->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $produto->titulo }}</td>
            <td><a href="{{ route('painel.produtos.variacoes.index', $produto->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.produtos.destroy', $produto->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.produtos.edit', $produto->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection