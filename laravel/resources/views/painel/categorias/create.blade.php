@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Categorias /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.categorias.store', 'files' => true]) !!}

        @include('painel.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
