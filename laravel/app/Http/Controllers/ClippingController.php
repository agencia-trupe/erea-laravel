<?php

namespace App\Http\Controllers;

use App\Models\Clipping;
use App\Models\ClippingImagem;

class ClippingController extends Controller
{
    public function index()
    {
        $clippings = Clipping::ordenados()->get();

        return view('frontend.clipping', compact('clippings'));
    }

    public function show($id)
    {
        $clipping = Clipping::find($id);
        $imagens = ClippingImagem::where('clipping_id', $id)->ordenados()->get();

        return view('frontend.clipping-imagens', compact('clipping', 'imagens'));
    }
}
