@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('colecao', $colecao->titulo) !!}
    {!! Form::hidden('colecao_id', $colecao->id) !!}
</div>

<div class="form-group">
    <label for="categorias">Categorias</label>
    <select name="categorias" id="categorias" class="form-control">
        <option value="" selected>Selecione</option>
        @foreach($categorias as $categoria)
        <option value="{{ $categoria->id }}">{{ $categoria->titulo }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="produto_id">Produtos</label>
    <select name="produto_id" id="produtosCat" class="form-control">

    </select>
</div>

<div class="well form-group">
    {!! Form::label('capa_alternativa,', 'Capa Alternativa (Se vazia, será usada a imagem de capa do produto)') !!}
    {!! Form::file('capa_alternativa', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.colecoes.produtos.index', $colecao->id) }}" class="btn btn-default btn-voltar">Voltar</a>