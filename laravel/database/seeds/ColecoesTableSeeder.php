<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColecoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('colecoes')->insert([
            'id' => 1,
            'titulo' => 'Coleção 2018',
            'slug' => 'colecao-2018',
        ]);
        DB::table('colecoes')->insert([
            'id' => 2,
            'titulo' => 'Coleção 2019',
            'slug' => 'colecao-2019',
        ]);
        DB::table('colecoes')->insert([
            'id' => 3,
            'titulo' => 'Coleção 2020',
            'slug' => 'colecao-2020',
        ]);
    }
}
