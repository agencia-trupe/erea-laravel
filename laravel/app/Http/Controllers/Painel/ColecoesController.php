<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ColecoesRequest;
use App\Models\Colecao;

class ColecoesController extends Controller
{
    public function index()
    {
        $registros = Colecao::orderBy('titulo', 'desc')->get();

        return view('painel.colecoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.colecoes.create');
    }

    public function store(ColecoesRequest $request)
    {
        try {

            $input = $request->all();

            Colecao::create($input);

            return redirect()->route('painel.colecoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Colecao $registro)
    {
        return view('painel.colecoes.edit', compact('registro'));
    }

    public function update(ColecoesRequest $request, Colecao $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.colecoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Colecao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.colecoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
