<ul class="nav navbar-nav">
    <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banner</a>
    </li>
    <li @if(Tools::routeIs('painel.institucional*')) class="active" @endif>
        <a href="{{ route('painel.institucional.index') }}">Institucional</a>
    </li>
    <li @if(Tools::routeIs('painel.categorias*')) class="active" @endif>
        <a href="{{ route('painel.categorias.index') }}">Categorias</a>
    </li>
    <li @if(Tools::routeIs('painel.produtos*')) class="active" @endif>
        <a href="{{ route('painel.produtos.index') }}">Produtos</a>
    </li>
    <li @if(Tools::routeIs('painel.colecoes*')) class="active" @endif>
        <a href="{{ route('painel.colecoes.index') }}">Coleções</a>
    </li>
    <li @if(Tools::routeIs('painel.clipping*')) class="active" @endif>
        <a href="{{ route('painel.clipping.index') }}">Clipping</a>
    </li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
</ul>