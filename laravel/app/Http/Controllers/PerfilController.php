<?php

namespace App\Http\Controllers;

use App\Models\Institucional;

class PerfilController extends Controller
{
    public function index()
    {
        $institucional = Institucional::first();

        return view('frontend.perfil', compact('institucional'));
    }
}
