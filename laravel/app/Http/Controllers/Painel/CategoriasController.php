<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriasRequest;
use App\Models\Categoria;

class CategoriasController extends Controller
{
    public function index()
    {
        $registros = Categoria::orderBy('titulo', 'asc')->get();

        return view('painel.categorias.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.categorias.create');
    }

    public function store(CategoriasRequest $request)
    {
        try {

            $input = $request->all();

            Categoria::create($input);

            return redirect()->route('painel.categorias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Categoria $registro)
    {
        return view('painel.categorias.edit', compact('registro'));
    }

    public function update(CategoriasRequest $request, Categoria $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.categorias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Categoria $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.categorias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
