@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Coleções /</small> Adicionar Coleção</h2>
    </legend>

    {!! Form::open(['route' => 'painel.colecoes.store', 'files' => true]) !!}

        @include('painel.colecoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
