<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InstitucionalTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('institucional')->insert([
            'imagem'    => '',
            'texto' => '<p>Criada em 1998 pelos arquitetos Fábio Berbari e Ricardo Minelli, a Érea surgiu de uma parceria da dupla que trabalhou junta em um grande escritório de arquitetura. Com a experiência adquirida por Ricardo, que ficou um ano em Roma estudando design mobiliário e a expertise de Fabio em aeromodelismo, eles aliaram as técnicas aprendidas para a criação de um mobiliário contemporâneo e de alta qualidade. A partir de então, encontraram no design espaço ideal para expressarem suas criações de estilo contemporâneo e minimalista.</p>
            <p>Um espírito ousado que figura em linhas retas e combinações homogêneas de madeira, vidro, aço cromado e escovado traduzem a identidade da marca que privilegia mais do que um estilo, mas um conceito de viver bem. A proposta da Érea é oferecer opções diferenciadas e exclusivas com desenhos inovadores e proporções adequadas, com riqueza de detalhes em resultado primoroso. Funcionalidade, bom gosto e acabamentos impecáveis também fazem parte de seus móveis completamente artesanais e fabricados com materiais nobres.</p>
            <p>A dupla, que possui em seu DNA características modernas, preza por oferecer aos clientes o que há de melhor e fazem questão de participar de todo o processo que vai desde a criação até a venda ao consumidor. Isso resulta em mobiliário sofisticado e design que transcende o rigor da funcionalidade até ser expresso como arte.</p>
            <p>Todos os móveis comercializados pela Érea são desenhados pela dupla e 100% produzidos em fábrica própria, no interior de São Paulo.</p>
            <p>A loja está localizada na Alameda Gabriel Monteiro da Silva, endereço referência em design e decoração da cidade paulistana. Atualmente, a Érea disponibiliza mais de 200 itens, entre mesas, cadeiras, sofás, objetos de decoração e demais acessórios para casas e escritórios.</p>',
        ]);
    }
}
