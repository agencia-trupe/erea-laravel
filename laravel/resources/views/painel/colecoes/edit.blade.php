@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Coleções /</small> Editar Coleção</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.colecoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.colecoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
