@extends('frontend.common.template')

@section('content')

<div class="content colecoes">
    <div class="center">
        @foreach($colecoes as $colecao)
        <a href="{{ route('colecoes.show', $colecao->slug) }}" class="colecao">{{ $colecao->titulo }}</a>
        @endforeach
    </div>
</div>

@endsection