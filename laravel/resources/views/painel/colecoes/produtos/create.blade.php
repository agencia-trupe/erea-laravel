@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Coleções /</small> Adicionar Produtos</h2>
    </legend>

    {!! Form::model($colecao, [
        'route'  => ['painel.colecoes.produtos.store', $colecao->id],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.colecoes.produtos.form', ['submitText' => 'Adicionar'])

    {!! Form::close() !!}

@endsection