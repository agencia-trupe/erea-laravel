@extends('frontend.common.template')

@section('content')

<div class="content clipping-imagens">
    <div class="btn-voltar">
        <a href="{{ route('clipping') }}" class="btn-voltar">« voltar</a>
    </div>
    <div class="center">
        @foreach($imagens as $imagem)
        <img src="{{ asset('assets/img/clipping/'.$imagem->imagem) }}" class="clipping-img" alt="">
        @endforeach
    </div>
</div>

@endsection