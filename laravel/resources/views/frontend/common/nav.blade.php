<div class="itens-menu">
    <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>Home</a>
    <a href="{{ route('perfil') }}" @if(Tools::routeIs('perfil')) class="active" @endif>Perfil</a>
    
    <a href="{{ route('produtos') }}" @if(Tools::routeIs('produtos*')) class="active" @endif id="menuProdutos">Produtos</a>
    <!-- SUBMENU CATEGORIAS -->
    @if(Tools::routeIs('produtos.show') || Tools::routeIs('produtos.showProduto'))
    <ul class="submenu-categorias" style="display: none;">
        @foreach($categorias as $categoria)
        <li>
            <a href="{{ route('produtos.getProdutos', $categoria->slug) }}" class="categoria {{ Request::segment(2) == $categoria->slug ? 'active' : '' }}">{{ $categoria->titulo }}</a>
        </li>
        @endforeach
    </ul>
    @endif
    
    <a href="{{ route('colecoes') }}" @if(Tools::routeIs('colecoes*')) class="active" @endif id="menuColecoes">Coleções</a>
    <!-- SUBMENU COLECOES -->
    @if(Tools::routeIs('colecoes.show') || Tools::routeIs('produtos.showProduto'))
    <ul class="submenu-colecoes" style="display: none;">
        @foreach($colecoes as $colecao)
        <li>
            <a href="{{ route('colecoes.getProdutos', $colecao->slug) }}" class="colecao {{ Request::segment(2) == $colecao->slug ? 'active' : '' }}">{{ $colecao->titulo }}</a>
        </li>
        @endforeach
    </ul>
    @endif

    <a href="{{ route('clipping') }}" @if(Tools::routeIs('clipping*')) class="active" @endif>Clipping</a>
    <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>
    <div class="social">
        @if($contato->instagram)
        <a href="{{ $contato->instagram }}" target="_blank" class="instagram"></a>
        @endif
        @if($contato->facebook)
        <a href="{{ $contato->facebook }}" target="_blank" class="facebook"></a>
        @endif
        @if($contato->pinterest)
        <a href="{{ $contato->pinterest }}" target="_blank" class="pinterest"></a>
        @endif
    </div>
</div>