<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutosVariacoesRequest;
use App\Models\Produto;
use App\Models\ProdutoVariacoes;

class ProdutosVariacoesController extends Controller
{
    public function index(Produto $produto)
    {
        $variacoes = ProdutoVariacoes::where('produto_id', $produto->id)->ordenados()->get();
        // dd($variacoes);

        return view('painel.produtos.variacoes.index', compact('variacoes', 'produto'));
    }

    public function show(Produto $produto, ProdutoVariacoes $variacao)
    {
        return $variacao;
    }

    public function store(Produto $produto, ProdutosVariacoesRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = ProdutoVariacoes::uploadImagem();
            $input['produto_id'] = $produto->id;

            $variacao = ProdutoVariacoes::create($input);

            $view = view('painel.produtos.variacoes.imagem', compact('produto', 'variacao'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(Produto $produto, ProdutoVariacoes $variacao)
    {
        try {

            $variacao->delete();
            return redirect()->route('painel.produtos.variacoes.index', $produto)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

    public function clear(Produto $produto)
    {
        try {

            $produto->variacoes()->delete();
            return redirect()->route('painel.produtos.variacoes.index', $produto)
                ->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
