<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        $router->model('clipping', 'App\Models\Clipping');
        $router->model('imagens_clipping', 'App\Models\ClippingImagem');
        $router->model('produtos', 'App\Models\Produto');
        $router->model('variacoes_produtos', 'App\Models\ProdutoVariacoes');
        $router->model('institucional', 'App\Models\Institucional');
        $router->model('categorias', 'App\Models\Categoria');
        $router->model('colecoes', 'App\Models\Colecao');
        $router->model('produtos_colecoes', 'App\Models\ColecaoProdutos');
        $router->model('banners', 'App\Models\Banner');
        $router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');
        $router->model('politica-de-privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('aceite-de-cookies', 'App\Models\AceiteDeCookies');

        $router->bind('categoria_slug', function ($slug) {
            return \App\Models\Categoria::whereSlug($slug)->firstOrFail();
        });

        $router->bind('colecao_slug', function ($slug) {
            return \App\Models\Colecao::whereSlug($slug)->firstOrFail();
        });

        $router->bind('produto_slug', function ($slug) {
            return \App\Models\Produto::whereSlug($slug)->firstOrFail();
        });

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
