<footer>
    <div class="footer">
        <p class="direitos">© {{ date('Y') }} {{ config('app.name') }} • Todos os direitos reservados</p>
        <div class="trupe">
            <a class="link" href="http://www.trupe.net" target="_blank">Criação de sites: Trupe Agência Criativa</a>
            <img class="kombi" src="{{ asset('assets/img/layout/kombi-trupe.png') }}" alt="">
        </div>
    </div>
</footer>