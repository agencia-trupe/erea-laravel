<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClippingImagensRequest;
use App\Models\Clipping;
use App\Models\ClippingImagem;

class ClippingImagensController extends Controller
{
    public function index(Clipping $registro)
    {
        $imagens = ClippingImagem::clipping($registro->id)->ordenados()->get();

        return view('painel.clipping.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Clipping $registro, ClippingImagem $imagem)
    {
        return $imagem;
    }

    public function store(Clipping $registro, ClippingImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = ClippingImagem::uploadImagem();
            $input['clipping_id'] = $registro->id;

            $imagem = ClippingImagem::create($input);

            $view = view('painel.clipping.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(Clipping $registro, ClippingImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.clipping.imagens.index', $registro)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

    public function clear(Clipping $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.clipping.imagens.index', $registro)
                ->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
