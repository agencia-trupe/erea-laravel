@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Editar Produto</h2>
    </legend>

    {!! Form::model($produto, [
        'route'  => ['painel.produtos.update', $produto->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
