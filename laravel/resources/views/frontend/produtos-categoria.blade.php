@extends('frontend.common.template')

@section('content')

<div class="content produtos-categoria">
    <div class="caminho">
        <p class="pt-prod">produtos |</p>
        <p class="pt-categ">{{ $categoriaFind->titulo }}</p>
        <span class="loading" style="display:none;"></span>
    </div>
    <div class="produtos">
        @foreach($produtos as $produto)
        <a href="{{ route('produtos.showProduto', ['produto' => $produto->slug]) }}" class="produto" title="{{ $produto->titulo }}">
            <img src="{{ asset('assets/img/produtos/'.$produto->imagem) }}" alt="{{ $produto->titulo }}">
        </a>
        @endforeach
    </div>
</div>

@endsection