<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome' => 'Érea',
            'email' => 'contato@erea.com.br',
            'telefone' => '11 3062.8590',
            'endereco' => 'Al. Gabriel Monteiro da Silva, 2158',
            'cep' => '01442-001',
            'cidade' => 'São Paulo SP',
            'instagram' => 'http://www.instagram.com/erea.casa',
            'facebook' => 'https://www.facebook.com/erea.casa',
            'pinterest' => 'https://br.pinterest.com/ereacasa/',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.827200322005!2d-46.68820090065499!3d-23.57464885288292!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5772fa0446c3%3A0xcc290ae416ae0bca!2sAlameda%20Gabriel%20Monteiro%20da%20Silva%2C%202158%20-%20Jardim%20America%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2001442-002!5e0!3m2!1spt-BR!2sbr!4v1599684645012!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
        ]);
    }
}
