<?php

namespace App\Http\Controllers;

use App\Models\Colecao;
use App\Models\ColecaoProdutos;
use App\Models\Produto;

class ColecoesController extends Controller
{
    public function index()
    {
        $colecoes = Colecao::orderBy('titulo', 'desc')->get();

        return view('frontend.colecoes', compact('colecoes'));
    }

    public function show($colecao)
    {
        $colecaoFind = Colecao::where('slug', $colecao)->first();

        $produtos = Colecao::where('colecoes.slug', $colecao)
            ->join('colecoes_produtos', 'colecoes_produtos.colecao_id', '=', 'colecoes.id')
            ->join('produtos', 'colecoes_produtos.produto_id', '=', 'produtos.id')
            ->orderBy('colecoes_produtos.ordem', 'ASC')
            ->get();
        
        return view('frontend.colecoes-produtos', compact('colecaoFind', 'produtos'));
    }

    public function getProdutos($colecao)
    {
        $colecaoFind = Colecao::where('slug', $colecao)->first();

        $produtos = Colecao::where('colecoes.slug', $colecao)
            ->join('colecoes_produtos', 'colecoes_produtos.colecao_id', '=', 'colecoes.id')
            ->join('produtos', 'colecoes_produtos.produto_id', '=', 'produtos.id')
            ->orderBy('colecoes_produtos.ordem', 'ASC')
            ->get();

        return response()->json(['produtos' => $produtos, 'colecao' => $colecaoFind]);
    }
}
