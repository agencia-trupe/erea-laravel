<div class="imagem col-md-2 col-sm-3 col-xs-4" style="margin:5px 0;position:relative;padding:0 5px;" id="{{ $variacao->id }}" data-ordem="{{ $variacao->ordem or 0 }}" data-imagem="{{ $variacao->imagem }}">
    <img src="{{ url('assets/img/produtos/thumbs/'.$variacao->imagem) }}" alt="" style="display:block;width:100%;height:auto;cursor:move;">
    {!! Form::open([
        'route'  => ['painel.produtos.variacoes.destroy', $produto->id, $variacao->id],
        'method' => 'delete'
    ]) !!}

    <div class="btn-group btn-group-sm" style="position:absolute;bottom:8px;left:10px;">
        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove"></span></button>
    </div>

    {!! Form::close() !!}
</div>
