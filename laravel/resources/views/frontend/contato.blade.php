@extends('frontend.common.template')

@section('content')

<div class="content contato">
    <div class="contato-mapa">
        {!! $contato->google_maps !!}
    </div>

    <div class="contato-form">
        <p class="telefone">{{ $contato->telefone }}</p>
        <div class="end-completo">
            <p>{{ $contato->endereco }}</p>
            <p>{{ $contato->cep }} {{ $contato->cidade }}</p>
        </div>
        <h2 class="fale-conosco">Fale Conosco</h2>
        <form action="{{ route('contato.post') }}" method="POST">
            {!! csrf_field() !!}
            <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
            <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="telefone">
            <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>

            <button type="submit" class="btn-contato">Enviar »</button>

            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                <p>Mensagem enviada com sucesso!</p>
            </div>
            @endif
        </form>
    </div>
</div>

@endsection