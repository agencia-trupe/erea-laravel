<header>
    <div class="center">
        <a href="{{ route('home') }}" class="logo" alt="{{ config('app.name') }}"></a>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
        <nav>
            @include('frontend.common.nav')
        </nav>
    </div>
</header>