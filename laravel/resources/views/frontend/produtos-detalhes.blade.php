@extends('frontend.common.template')

@section('content')

<div class="content produtos-detalhes">
    <div class="nav-produtos">
        <div class="caminho">
            <p class="pt-prod">produtos |</p>
            <p class="pt-categ">{{ $categoriaFind->titulo }}</p>
        </div>
        <a href="{{ route('produtos.show', $categoriaFind->slug) }}" class="btn-voltar">« voltar</a>
    </div>

    <div class="detalhes">
        <div class="detalhes-img">
            @if($variacoes && isset($variacoes[0]))
            <img src="{{ asset('assets/img/produtos/'.$variacoes[0]->imagem) }}" alt="">
            @endif
        </div>
        <div class="detalhes-variacoes">
            <div class="variacoes">
                <a class="nav nav-up" href="#"></a>
                <div class="itens-variacoes">
                    @foreach($variacoes as $key => $variacao)
                    @if($key == 0)
                    <a href="{{ asset('assets/img/produtos/'.$variacao->imagem) }}" class="variacao variacao-ativo">
                        <img src="{{ asset('assets/img/produtos/'.$variacao->imagem) }}" alt="">
                    </a>
                    @else
                    <a href="{{ asset('assets/img/produtos/'.$variacao->imagem) }}" class="variacao">
                        <img src="{{ asset('assets/img/produtos/'.$variacao->imagem) }}" alt="">
                    </a>
                    @endif
                    @endforeach
                </div>
                <a class="nav nav-down" href="#"></a>
            </div>
            <div class="detalhes-dados">
                <h2 class="titulo">{{ $produtoDetalhe->titulo }}</h2>
                <div class="texto">
                    {!! $produtoDetalhe->texto !!}
                </div>
            </div>
        </div>
    </div>

</div>

@endsection